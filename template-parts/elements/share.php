<?php 
/*----------------------------------------------------------------*\

	DISPLAY 0-3 RELATED POSTS BY CATEGORY

\*----------------------------------------------------------------*/
?>

<?php 
	$post_url = get_permalink();
	$post_content = get_the_excerpt();
	$featured_img = get_home_url() . get_the_post_thumbnail_url();
	$post_title = get_the_title();
?>

<section class="social-share">
	<h4>Share this post</h4>
	<a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $post_url; ?>" class="button is-icon is-ghost is-borderless" title="Share on Facebook">
		<svg role="img" aria-labelledby="facebook-title" focusable="false">
			<title id="facebook-title">Facebook Icon</title>
			<use xlink:href="#facebook" />
		</svg>
	</a>
	<a target="_blank" href="https://twitter.com/home?status=<?php echo $post_content; echo $post_url; ?>" class="button is-icon is-ghost is-borderless" title="Share on Twitter">
		<svg role="img" aria-labelledby="twitter-title" focusable="false">
			<title id="twitter-title">Twitter Icon</title>
			<use xlink:href="#twitter" />
		</svg>
	</a>
	<a target="_blank" href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo $post_url; ?>&summary=<?php echo $post_content; ?>" class="button is-icon is-ghost is-borderless" title="Share on Linkedin">
		<svg role="img" aria-labelledby="linkedin-title" focusable="false">
			<title id="linkedin-title">LinkedIn Icon</title>
			<use xlink:href="#linkedin" />
		</svg>
	</a>
	<a target="_blank" href="https://pinterest.com/pin/create/button/?url=<?php echo $post_url; ?>&media=<?php echo $featured_img; ?>&description=<?php echo $post_content; ?>" class="button is-icon is-ghost is-borderless" title="Share on Pinterest">
		<svg role="img" aria-labelledby="pinterest-title" focusable="false">
			<title id="pinterest-title">Pinterest Icon</title>
			<use xlink:href="#pinterest" />
		</svg>
	</a>
	<a target="_blank" href="mailto:?&subject=Check out this post <?php echo $post_title; ?>&body=<?php echo $post_url; echo $post_url; ?>" class="button is-icon is-ghost is-borderless" title="Share on Pinterest">
		<svg role="img" aria-labelledby="email-title" focusable="false">
			<title id="email-title">Email Icon</title>
			<use xlink:href="#email" />
		</svg>
	</a>
</section>