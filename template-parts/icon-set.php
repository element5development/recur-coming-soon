<?php 
/*----------------------------------------------------------------*\

	SVG ICONS
	List of all the svg icons used throughout the site.
	Allows for easy access without repeat code.

\*----------------------------------------------------------------*/
?>

<svg xmlns="http://www.w3.org/2000/svg" style="display: none">
	<symbol id="close" viewBox="0 0 64 64">
		<circle data-name="layer2" cx="32.001" cy="32" r="30" transform="rotate(-45 32.001 32)" stroke-miterlimit="10" stroke-width="2"
		  stroke-linejoin="round" stroke-linecap="round"></circle>
		<path data-name="layer1" stroke-miterlimit="10" stroke-width="2" d="M42.999 21.001l-22 22m22 0L21 21" stroke-linejoin="round"
		  stroke-linecap="round"></path>
	</symbol>
	<symbol id="arrowhead-right" viewBox="0 0 64 64">
		<path data-name="layer1" stroke-miterlimit="10" stroke-width="4" d="M26 20.006L40 32 26 44.006" stroke-linejoin="round" stroke-linecap="round"></path>
	</symbol>
	<symbol id="arrowhead-left" viewBox="0 0 64 64">
		<path data-name="layer1" stroke-miterlimit="10" stroke-width="4" d="M40 44.006L26 32.012l14-12.006" stroke-linejoin="round"
		  stroke-linecap="round"></path>
	</symbol>
	<symbol id="arrowhead-down" viewBox="0 0 64 64">
		<path data-name="layer1" stroke-miterlimit="10" stroke-width="2" d="M20 26l11.994 14L44 26" stroke-linejoin="round" stroke-linecap="round"></path>
	</symbol>
	<symbol id="logo" viewBox="0 0 64 64">
		<path data-name="layer2" d="M11.1 47.3C5.4 50 1.7 54.9 2 62c7.1.4 12-3.4 14.7-9.1" stroke-linecap="round" stroke-linejoin="round"
		  stroke-width="2"></path>
		<path data-name="layer1" d="M62 2s-13.4-.7-22.6 8.5S8.3 44.4 8.3 44.4l11.3 11.4s24.7-22 33.9-31.2S62 2 62 2z" stroke-linecap="round"
		  stroke-linejoin="round" stroke-width="2"></path>
		<circle data-name="layer2" cx="44" cy="20" r="4" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"></circle>
		<path data-name="layer1" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M13.6 38.5l12 11.9M24.8 26H12.5l-7.7 7.7 11 2.3M37 40.2v12.3l-7.7 7.6L27 49.2"></path>
	</symbol>
	<symbol id="facebook" viewBox="0 0 64 64">
		<path data-name="layer1" d="M39.312 13.437H47V2h-9.094C26.938 2.469 24.688 8.656 24.5 15.125v5.719H17V32h7.5v30h11.25V32h9.281l1.781-11.156H35.75v-3.469a3.714 3.714 0 0 1 3.562-3.938z" fill="none" stroke-linecap="round" stroke-miterlimit="10" stroke-width="2"/>
	</symbol>
	<symbol id="twitter" viewBox="0 0 64 64">
		<path data-name="layer1" d="M60.448 15.109a24.276 24.276 0 0 1-3.288.968.5.5 0 0 1-.451-.853 15.146 15.146 0 0 0 3.119-4.263.5.5 0 0 0-.677-.662 18.6 18.6 0 0 1-6.527 2.071 12.92 12.92 0 0 0-9-3.75A12.363 12.363 0 0 0 31.25 20.994a12.727 12.727 0 0 0 .281 2.719c-9.048-.274-19.61-4.647-25.781-12.249a.5.5 0 0 0-.83.073 12.475 12.475 0 0 0 2.956 14.79.5.5 0 0 1-.344.887 7.749 7.749 0 0 1-3.1-.8.5.5 0 0 0-.725.477 11.653 11.653 0 0 0 7.979 10.567.5.5 0 0 1-.09.964 12.567 12.567 0 0 1-2.834 0 .506.506 0 0 0-.536.635c.849 3.282 5.092 7.125 9.839 7.652a.5.5 0 0 1 .267.87 20.943 20.943 0 0 1-14 4.577.5.5 0 0 0-.255.942 37.29 37.29 0 0 0 17.33 4.266 34.5 34.5 0 0 0 34.687-36.182v-.469a21.11 21.11 0 0 0 4.934-4.839.5.5 0 0 0-.58-.765z" fill="none" stroke-linecap="round" stroke-miterlimit="10" stroke-width="2"/>
	</symbol>
	<symbol id="linkedin" viewBox="0 0 64 64">
		<path data-name="layer1" fill="none" stroke-linecap="round" stroke-miterlimit="10" stroke-width="2" d="M3.078 22.331h12.188v36.844H3.078z"/>
		<path data-name="layer2" d="M46.719 21.112c-5.344 0-8.531 1.969-11.906 6.281v-5.062H22.625v36.844h12.281V39.206c0-4.219 2.156-8.344 7.031-8.344s7.781 4.125 7.781 8.25v20.063H62V38.269c0-14.532-9.844-17.157-15.281-17.157z" fill="none" stroke-linecap="round" stroke-miterlimit="10" stroke-width="2"/>
		<path data-name="layer1" d="M9.219 4.425C5.188 4.425 2 7.331 2 10.894s3.188 6.469 7.219 6.469 7.219-2.906 7.219-6.469-3.188-6.469-7.219-6.469z" fill="none" stroke-linecap="round" stroke-miterlimit="10" stroke-width="2"/>
	</symbol>
	<symbol id="pinterest" viewBox="0 0 64 64">
		<path data-name="layer1" d="M32 1.994a30.016 30.016 0 0 0-11.906 57.563 24.712 24.712 0 0 1 .563-6.844c.563-2.437 3.844-16.406 3.844-16.406a12.129 12.129 0 0 1-.938-4.781c0-4.5 2.625-7.781 5.813-7.781 2.719 0 4.031 2.063 4.031 4.5 0 2.719-1.781 6.844-2.625 10.688a4.677 4.677 0 0 0 4.781 5.813c5.719 0 9.563-7.312 9.563-16.031 0-6.562-4.406-11.531-12.563-11.531-9.094 0-14.812 6.844-14.812 14.437a8.6 8.6 0 0 0 1.969 5.906 1.505 1.505 0 0 1 .469 1.687c-.188.562-.469 1.875-.656 2.437a1.04 1.04 0 0 1-1.5.75c-4.219-1.687-6.188-6.281-6.188-11.531 0-8.531 7.219-18.844 21.563-18.844 11.531 0 19.031 8.344 19.031 17.25 0 11.812-6.562 20.625-16.219 20.625-3.281 0-6.281-1.781-7.312-3.75 0 0-1.781 6.938-2.156 8.25a26.631 26.631 0 0 1-3 6.375A26.63 26.63 0 0 0 32 61.994a30 30 0 1 0 0-60z" fill="none" stroke-linecap="round" stroke-miterlimit="10" stroke-width="2" stroke-linejoin="round"/>
	</symbol>
	<symbol id="email" viewBox="0 0 64 64">
		<path data-name="layer2" fill="none" stroke-miterlimit="10" stroke-width="2" d="M2 12l30 27.4L62 12" stroke-linejoin="round" stroke-linecap="round"/>
		<path data-name="layer1" fill="none" stroke-miterlimit="10" stroke-width="2" d="M2 12h60v40H2z" stroke-linejoin="round" stroke-linecap="round"/>
	</symbol>
	<symbol id="phone" viewBox="0 0 64 64">
		<path data-name="layer1" d="M58.9 47l-10.4-6.8a4.8 4.8 0 0 0-6.5 1.3c-2.4 2.9-5.3 7.7-16.2-3.2S19.6 24.4 22.5 22a4.8 4.8 0 0 0 1.3-6.5L17 5.1c-.9-1.3-2.1-3.4-4.9-3S2 6.6 2 15.6s7.1 20 16.8 29.7S39.5 62 48.4 62s13.2-8 13.5-10-1.7-4.1-3-5z" fill="none" stroke="#202020" stroke-miterlimit="10" stroke-width="2" stroke-linejoin="round" stroke-linecap="round"/>
	</symbol>
</svg>