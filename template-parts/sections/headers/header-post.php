<?php 
/*----------------------------------------------------------------*\

	DEFAULT HEADER
	Most basic and simple page title

\*----------------------------------------------------------------*/
?>

<header class="page-title">
	<section>

		<h1>
			<?php 
				if ( get_field('post_title') ) :
					the_field('post_title');
				else :
					the_title();
				endif;
			?>
		</h1>

		<div class="post-meta">
			<div>
				<?php if ( get_field('author') ) : ?>
					<?php $author = get_field('author'); ?>
					<?php echo $author['user_avatar']; ?>
				<?php endif; ?>
			</div>
			<div>
				<?php if ( get_field('author') ) : ?>
					<?php $author = get_field('author'); ?>
					<p><?php echo $author['display_name']; ?> </p>
				<?php endif; ?>
				<p><?php echo get_the_date('M d, Y'); ?></p>
				<p><?php $categories = get_the_category($post->ID); echo $categories[0]->name; ?></p>
			</div>
		</div>

	</section>
</header>