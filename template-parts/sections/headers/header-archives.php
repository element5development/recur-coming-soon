<?php 
/*----------------------------------------------------------------*\

	HEADER FOR ALL ARCHIVES
	Used for any/all post types and search results

\*----------------------------------------------------------------*/
?>

<?php 
	//GET POST TYPE FROM PAGE
	//EMPTY FOR DEFAULT BLOG
	$posttype = get_query_var('post_type');

	if ( $posttype == '' ) {
		$posttype = 'blog';
	}
?>

<header class="page-title has-image" style="background-image: url('<?php the_field( $posttype . '_title_bg_img', 'option'); ?>');">
	<section>

		<h1><?php the_field( $posttype . '_page_title', 'option'); ?></h1>
		<?php if ( get_field( $posttype . '_title_description', 'option') ) : ?>
			<p class="subheader"><?php the_field( $posttype . '_title_description', 'option'); ?></p>
		<?php endif; ?>

	</section>

	<div class="overlay"></div>
</header>