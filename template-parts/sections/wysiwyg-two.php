<?php 
/*----------------------------------------------------------------*\

	WYSIWYG THAT HAS TWO COLUMNS

\*----------------------------------------------------------------*/
?>

<section class="wysiwyg-block">
	<?php if ( get_sub_field('title') ) : ?>
		<h2><?php the_sub_field('title'); ?></h2>
	<?php endif; ?>
	<div>
		<?php the_sub_field('left_wysiwyg'); ?>
	</div>
	<div>
		<?php the_sub_field('right_wysiwyg'); ?>
	</div>
</section>