<?php 
/*----------------------------------------------------------------*\

	DISPLAY ALL AVAILABLE TICKET OPTIONS

\*----------------------------------------------------------------*/
?>


<section class="ticket-selections">
	<h2>Limited Tickets Available</h2>
	<p>Super Early Bird Pricing ends soon!</p>
	<div class="tickets">
		<div class="ticket">
			<p>Super Early Bird Pricing</p>
			<h3>Merchants</h3>
			<p>For merchants within the subscription industry.</p>
			<p class="price">$300</p>
			<p>SUBTA Member Price</p>
			<a target="_blank" rel="noopener noreferrer" href="https://my.subta.com/upcoming-events" class="button is-white">Get Tickets Now</a>
			<div class="arcs"></div>
			<svg data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 360 450"><defs><linearGradient id="a" x1="340.1" y1="430.4" x2="28.18" y2="30.23" gradientUnits="userSpaceOnUse"><stop offset="0" stop-color="#e80000"/><stop offset="1" stop-color="#8f003d"/></linearGradient></defs><path d="M0 42.06V10A10 10 0 0 1 10 0h340a10 10 0 0 1 10 10v32a12 12 0 0 0 0 24v374a10 10 0 0 1-10 10H10a10 10 0 0 1-10-10V65.94a12 12 0 1 0 0-23.88z" fill="url(#a)"/></svg>
		</div>
		<div class="ticket">
			<p>Super Early Bird Pricing</p>
			<h3>Partners & Sponsors</h3>
			<p>For suppliers and partners of merchants in the subscription commerce ecomony.</p>
			<p class="price">$1000</p>
			<p>SUBTA Member Price</p>
			<a target="_blank" rel="noopener noreferrer" href="https://my.subta.com/upcoming-events" class="button is-white">Get Tickets Now</a>
			<div class="arcs"></div>
			<svg data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 360 450"><defs><linearGradient id="b" x1="31.66" y1="34.53" x2="330.54" y2="418.3" gradientUnits="userSpaceOnUse"><stop offset="0" stop-color="#15c4ca"/><stop offset="1" stop-color="#75c583"/></linearGradient></defs><path d="M0 42.06V10A10 10 0 0 1 10 0h340a10 10 0 0 1 10 10v32a12 12 0 0 0 0 24v374a10 10 0 0 1-10 10H10a10 10 0 0 1-10-10V65.94a12 12 0 1 0 0-23.88z" fill="url(#b)"/></svg>
		</div>
	</div>
	<h2>Interested in becoming a sponsor?</h2>
	<a target="_blank" rel="noopener noreferrer" href="https://calendly.com/jpbrophy/subta" class="button">Speak with our team</a>
	<svg id="arrow" height="469" width="295" xmlns="http://www.w3.org/2000/svg"><linearGradient id="c" x1="0%" y1="67.683%" y2="37.294%"><stop offset="0" stop-color="#15c4ca"/><stop offset="1" stop-color="#75c583"/></linearGradient><path d="M27.007 367.995l-32.189 32.189L-9 271.41l128.774 3.818-30.743 30.743c13.771 13.262 30.91 23.453 50.591 29.097 64.084 18.376 130.858-18.428 149.145-82.202 12.741-44.433-1.416-90.236-32.849-120.146L317.638 71c81.537 81.537 81.856 213.415.712 294.558-80.085 80.086-209.59 80.82-291.343 2.437z" fill="url(#c)" opacity=".8" transform="rotate(130 185 248.604)"/></svg>
	<svg id="halfcircle" height="329" width="295" xmlns="http://www.w3.org/2000/svg"><linearGradient id="d" x1="61.336%" x2="38.787%" y1="7.069%" y2="95.471%"><stop offset="0" stop-color="#fe941f"/><stop offset="1" stop-color="#e80000"/></linearGradient><path d="M-6 267.062L234.895 25c66.521 66.844 66.521 175.218 0 242.062-66.521 66.844-174.374 66.844-240.895 0z" fill="url(#d)" opacity=".8" transform="rotate(-20 139.393 171.097)"/></svg>
	<svg id="circle" height="160" width="160" xmlns="http://www.w3.org/2000/svg"><linearGradient id="e" x1="25.525%" y1="18.718%" y2="100%"><stop offset="0" stop-color="#75c583"/><stop offset="1" stop-color="#007e7b"/></linearGradient><circle cx="79.725" cy="79.725" fill="url(#e)" fill-rule="evenodd" r="79.725"/></svg>
</section>