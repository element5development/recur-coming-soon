<?php 
/*----------------------------------------------------------------*\

	SLIDER FOR ICONS 
	commonly used for awards and partners

\*----------------------------------------------------------------*/
?>

<section class="icon-slider">
	<h2><?php the_sub_field('logo_section_title'); ?></h2>
	<?php if( have_rows('logo_repeater') ): ?>
	<div class="icons">
		<?php while ( have_rows('logo_repeater') ) : the_row(); ?>
			<div class="icon">
				<?php $image = get_sub_field('logo'); ?>
				<img src="<?php echo $thumb = $image['sizes']['small']; ?>" alt="<?php echo $image['alt'] ?>" />
			</div>
		<?php endwhile; ?>
	</div>
	<?php endif; ?>
</section>