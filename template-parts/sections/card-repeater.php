<?php 
/*----------------------------------------------------------------*\

	CARD REPEATER SECTION

\*----------------------------------------------------------------*/
?>

<section class="card-repeater">
	<div>
		<?php if ( get_sub_field('section_title') ) : ?>
			<h3><?php the_sub_field('section_title'); ?></h3>
		<?php endif; ?>
		
		<?php while ( have_rows('card_repeater') ) : the_row(); ?>
			<article class="preview-card">
				<?php $image = get_sub_field('headshot'); ?>
				<img src="<?php echo $image['sizes']['small']; ?>" alt="<?php echo $image['alt']; ?>" />
				<div class="speaker-info">
					<h5><?php the_sub_field('name'); ?></h5>
					<p class="position"><?php the_sub_field('position'); ?></p>
					<p class="company"><?php the_sub_field('company'); ?></p>
				</div>
			</article>
		<?php endwhile; ?>
	</div>
</section>