<?php 
/*----------------------------------------------------------------*\

	TESTIMONIALS SECTION

\*----------------------------------------------------------------*/
?>

<section class="testimonials">
	<div class="block">
		<h2><?php the_sub_field('testimonial_title'); ?></h2>
		<p><?php the_sub_field('testimonial_p'); ?></p>
		<div class="testimonial-slider">
			<?php if( have_rows('testimonial_slider') ): ?>
				<?php while ( have_rows('testimonial_slider') ) : the_row(); ?>
					<article class="testimonial">
						<blockquote>
							<p>"<?php the_sub_field('testimonial'); ?>"</p>
						</blockquote>
					</article>
				<?php endwhile; ?>
			<?php endif; ?>
		</div>
	</div>
</section>