<?php 
/*----------------------------------------------------------------*\

	RIGHT SIDEBAR
	Used on the sidebar-both and sidebar-right templates

\*----------------------------------------------------------------*/
?>

<aside class="is-right">
	<?php dynamic_sidebar('right'); ?>
</aside>