<?php 
/*----------------------------------------------------------------*\

	HOME/FRONT PAGE TEMPLATE
	Customized home page commonly composed of various reuseable sections.

\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<main>
	<article>
		<?php the_content(); ?>
	</article>
</main>

<?php
	if( have_rows('article') ):
		while ( have_rows('article') ) : the_row();

			if( get_row_layout() == 'basic_editor' ):
				get_template_part('template-parts/sections/wysiwyg');
			elseif( get_row_layout() == 'two_col_editor' ): 
				get_template_part('template-parts/sections/wysiwyg-two');
			elseif( get_row_layout() == 'banner' ): 
				get_template_part('template-parts/sections/banner');
			elseif( get_row_layout() == 'gallery' ): 
				get_template_part('template-parts/sections/gallery');
			elseif( get_row_layout() == 'card_repeater' ): 
				get_template_part('template-parts/sections/card-repeater');
			elseif( get_row_layout() == 'logo_repeater' ): 
				get_template_part('template-parts/sections/logo-repeater');
			elseif( get_row_layout() == 'testimonials' ): 
				get_template_part('template-parts/sections/testimonials');
			elseif( get_row_layout() == 'map' ): 
				get_template_part('template-parts/sections/map');
			elseif( get_row_layout() == 'tickets' ): 
				get_template_part('template-parts/sections/tickets');
			endif;

		endwhile;
	endif; 
?>

<?php get_template_part('template-parts/sections/footers/footer'); ?>

<?php get_footer(); ?>