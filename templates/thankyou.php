<?php 
/*----------------------------------------------------------------*\

	Template Name: Thank You
	
\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<main>
	<article>
		<section class="wysiwyg-block">
			<h1><?php the_field('page_title'); ?></h1>
			<p><?php the_field('title_description'); ?></p>
		</section>
	</article>
</main>

<?php get_footer(); ?>