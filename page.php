<?php 
/*----------------------------------------------------------------*\

	DEFAULT PAGE TEMPLATE
	Standard page template for website which should include all the
	commonly used options.

\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<?php
	if ( get_field('title_bg_vid') ) :
		get_template_part('template-parts/sections/headers/header-video');
	else :
		get_template_part('template-parts/sections/headers/header');
	endif;
?>


<?php if ( function_exists('yoast_breadcrumb') ) { 
	yoast_breadcrumb('<nav class="breadcrumbs">','</nav>'); 
} ?>

<?php 
	if ( get_field('sidebar') == 'both') :
		$asideClass = 'aside-both';
	elseif ( get_field('sidebar') == 'right') :
		$asideClass = 'aside-right';
	elseif ( get_field('sidebar') == 'left') :
		$asideClass = 'aside-left';
	else :
		$asideClass = '';
	endif;
?>

<main class="<?php echo $asideClass; ?>">
	<article class="<?php echo $asideClass; ?>">
		<?php
			if( have_rows('article') ):
				while ( have_rows('article') ) : the_row();

					if( get_row_layout() == 'basic_editor' ):
						get_template_part('template-parts/sections/wysiwyg');
					elseif( get_row_layout() == 'two_col_editor' ): 
						get_template_part('template-parts/sections/wysiwyg-two');
					elseif( get_row_layout() == 'banner' ): 
						get_template_part('template-parts/sections/banner');
					elseif( get_row_layout() == 'gallery' ): 
						get_template_part('template-parts/sections/gallery');
					endif;

				endwhile;
			endif; 
		?>
	</article>
	<?php 
		if ( get_field('sidebar') == 'left' || get_field('sidebar') == 'both' ) :
			get_template_part('template-parts/sections/sidebars/sidebar-left');
		endif;
		if ( get_field('sidebar') == 'right' || get_field('sidebar') == 'both' ) :
			get_template_part('template-parts/sections/sidebars/sidebar-right');
		endif;
	?>
</main>

<?php get_template_part('template-parts/sections/footers/footer'); ?>

<?php get_footer(); ?>