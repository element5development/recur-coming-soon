<?php 
/*----------------------------------------------------------------*\

	ERROR / NO PAGE FOUND

\*----------------------------------------------------------------*/
?>


<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<main>
	<article>
		<section class="wysiwyg-block">
			<h1>Oops!</h1>
			<p>We can't seem to find the page you're looking for.</p>
			<div class="buttons">
				<a class="button is-primary" href="/">Return to the Homepage</a>
				<a class="button is-ghost" href="/">Contact Support</a>
			</div>
		</section>
	</article>
</main>

<?php get_template_part('template-parts/sections/footers/footer'); ?>

<?php get_footer(); ?>