var $ = jQuery;

$(document).ready(function () {
	/*----------------------------------------------------------------*\
  	ENTRANCE ANIMATIONS
	\*----------------------------------------------------------------*/
	emergence.init({
		offsetTop: 20,
		offsetRight: 20,
		offsetBottom: 20,
		offsetLeft: 20,
	});
	/*----------------------------------------------------------------*\
		FILE UPLOAD
	\*----------------------------------------------------------------*/
	if ($('input[type="file"]').length > 0) {
		var fileInput = document.querySelector("input[type='file']");
		var button = document.querySelector("input[type='file']+span");
		fileInput.addEventListener("change", function (event) {
			button.innerHTML = this.value;
			fileInput.classList.add("file-uploaded");
		});
	}
	/*----------------------------------------------------------------*\
  	LOGIC FOR LOAD MORE BUTTON
	\*----------------------------------------------------------------*/
	if ($('.next.page-numbers').length) {
		$('.load-more').css('display', 'table');
	}
	/*----------------------------------------------------------------*\
		INFINITE SCROLL INIT
	\*----------------------------------------------------------------*/
	$('.feed').infiniteScroll({
		path: '.next.page-numbers',
		append: '.preview',
		button: '.load-more',
		scrollThreshold: false,
		checkLastPage: true,
		status: '.page-load-status',
	});
	/*------------------------------------------------------------------
  	TESTIMONIAL SLIDER
  ------------------------------------------------------------------*/
	$('.testimonial-slider').slick({
		infinite: true,
		slidesToShow: 2,
		slidesToScroll: 2,
		prevArrow: '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32"><path d="M24.19 29.82a1.18 1.18 0 0 1 .39.91 1.21 1.21 0 0 1-.39.91 1.29 1.29 0 0 1-1.79 0L7.82 16.91a1.24 1.24 0 0 1 0-1.82L22.4.36a1.29 1.29 0 0 1 1.79 0 1.3 1.3 0 0 1 .39.93 1.17 1.17 0 0 1-.39.89L10.9 16z" fill="#404042" data-name="Layer 1"/></svg>',
		nextArrow: '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32"><path d="M7.81 2.18a1.18 1.18 0 0 1-.39-.91 1.21 1.21 0 0 1 .39-.91 1.29 1.29 0 0 1 1.79 0l14.58 14.73a1.24 1.24 0 0 1 0 1.82L9.6 31.64a1.29 1.29 0 0 1-1.79 0 1.3 1.3 0 0 1-.39-.93 1.17 1.17 0 0 1 .39-.89L21.1 16z" fill="#404042" data-name="Layer 1"/></svg>',
		responsive: [{
			breakpoint: 800,
			settings: {
				slidesToShow: 1,
				slidesToScroll: 1,
			}
		}, ]
	});
	/*----------------------------------------------------------------*\
		ICON SLIDER
	\*----------------------------------------------------------------*/
	$('.icons').slick({
		arrows: false,
		infinite: true,
		slidesToShow: 3,
		slidesToScroll: 1,
		autoplay: true,
		autoplaySpeed: 3000,
		responsive: [{
			breakpoint: 800,
			settings: {
				slidesToShow: 2,
			}
		}]
	});

});